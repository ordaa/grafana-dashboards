# ORDAA Grafana dashboards

## Description
This is a collections of grafana dashboards I made on my spare time. You can find them in the folder [grafana/](grafana/).

## Installation
- Login to your Grafana environment
- Go to Dashboards > Browse
- Click on "Import"
- Paste the JSON contents in "Import via panel json"
- Click on Load
- Profit!

It is likely that Grafana will not ask you to choose a datasource, because the dashboard includes a datasource parameter you can change on the fly.

## Screenshots
[Alertmanager](grafana/alertmanager.json):
![Alertmanager grafana dashboard screenshot](screenshots/ordaa_alertmanager.png)

## License
[GPL version 3](https://www.gnu.org/licenses/)
You can find a full copy of the license in the file [LICENSE](LICENSE).

## Copyright
Thomas Venieris 2023, all rights reserved.
